# OneToManyRelationship
A demo application that shows how to retrieve the parent Id or field from the child entity in a OneToMany bidirectional relationship

How To Retrieve a Parent Field From a Child Entity in a One To Many Bidirectional JPA Relationship in Spring Boot

Understanding database relationships is key to modeling any application. As a developer, more often than not, you’d encounter a ManyToOne/OneToMany relationship between entities and how well these relationships are mapped determines to a large extent the complexity of your application.

I recently encountered a problem in a project I am working on which was to retrieve the parent Id from the child entity in a OneToMany bidirectional relationship. During my research, I found quite a few “hacks”; one of which was to use a custom Java Persistence Query Language(JPQL) which queries the entities and returns a custom object. Another option was to use a simple getter method to return the parent field(Id or any parent field) in the child entity. We would consider the latter of the aforementioned fixes in this article!

In this tutorial we’ll consider a hypothetical bidirectional One-To-Many relationship between an Author and Books written. To fully understand how to model a OneToMany/ ManyToOne relationship, you can consider the following articles:
* [The best way to map a@OneToMany relationship by Vlad Mihalcea](https://vladmihalcea.com/the-best-way-to-map-a-onetomany-association-with-jpa-and-hibernate/)
* [JPA/Hibernate One to Many Mapping Example with Spring Boot by Rajeev Kumar Singh](https://www.callicoder.com/hibernate-spring-boot-jpa-one-to-many-mapping-example/)

The Author-Book model is shown below:

![Database Diagram](img/diagram.png "Database Diagram")

It follows that an Author can have one or more Books and many Books can be written by the same author.

I’ll be using the basic oracle database for this tutorial. to configure oracle database for Spring boot, checkout my earlier post.

Book Entity

Add the following to the book entity. Don’t forget to include the getters and setters. Here, the Book entity has a ManyToOne Annotation which is mapped by the authorId.

To retrieve the authorId(parent Id) from the Book(Child) entity, we simply add a getter method that returns the author id. \Also we can retrieve the author’s full name with a getAuthorName() method as shown below:

Since getter methods are used to fetch objects, any getter method like the ones we’ve added to return the author’s Id and full name is returned as part of the Book entity when it is fetched i.e. when we do a “findbyId” on the Book entity as we shall see later.

> Note the @JsonIgnore annotation on the getAuthor() method in the Book(child) entity. This is done to avoid an infinite recursion going on during serialization since Author refers to Book and Book refer to Author.

## Testing on Postman
First, we create an Author, so we can use the author’s id to create Books
a. Create Author:
POST → localhost:8080/author

create an Author
b. Create Book
POST → localhost:8080/{authorId}/book

create a Book entity by the author id
c. Get Book by Id
POST → localhost:8080/book/{bookId}
As can be seen the Author’s id and Author’s full name is part of the returned Book object

Get Book By Id
This article is inspired by the challenge I faced recently on a project and this “solution” could also suffice for this stackoverflow question. I’ve added an answer though.