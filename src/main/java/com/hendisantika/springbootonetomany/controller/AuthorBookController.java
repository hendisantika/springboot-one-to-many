package com.hendisantika.springbootonetomany.controller;

import com.hendisantika.springbootonetomany.entity.Author;
import com.hendisantika.springbootonetomany.entity.Book;
import com.hendisantika.springbootonetomany.entity.Child;
import com.hendisantika.springbootonetomany.entity.Parent;
import com.hendisantika.springbootonetomany.service.AuthorService;
import com.hendisantika.springbootonetomany.service.BookService;
import com.hendisantika.springbootonetomany.service.ChildService;
import com.hendisantika.springbootonetomany.service.ParentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-one-to-many
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/03/20
 * Time: 07.44
 */
@RestController
public class AuthorBookController {

    @Autowired
    private AuthorService authorService;

    @Autowired
    private BookService bookService;

    @Autowired
    private ParentService parentService;

    @Autowired
    private ChildService childService;

    @GetMapping(value = "/getAllAuthors")
    public List<Author> getAuthors() {
        return authorService.getAuthors();
    }

    @PostMapping(value = "/authors")
    public Author createAuthor(@RequestBody Author author) {
        return authorService.createAuthor(author);
    }

    @PostMapping(value = "/parents")
    public Parent createParent(@RequestBody Parent parent) {
        return parentService.createParent(parent);
    }

    @GetMapping(value = "/authors/{authorId}")
    public Optional<Author> getAuthorById(@PathVariable(value = "authorId") Long authorId) {
        return authorService.getAuthorById(authorId);
    }

    @PutMapping(value = "/authors")
    public Author updateAuthor(@PathVariable(value = "authorId") Long authorId, @RequestBody Author author) {
        return authorService.updateAuthorById(authorId, author);
    }

    @DeleteMapping(value = "/authors/{authorId}")
    public ResponseEntity<Object> deleteAuthorById(@PathVariable(value = "authorId") long authorId) {
        return authorService.deleteAuthorById(authorId);
    }

    @GetMapping(value = "/getAllBooks")
    public List<Book> getBooks() {
        return bookService.getAllBooks();
    }

    @PostMapping(value = "/{authorId}/book")
    public Book createBook(@PathVariable(value = "authorId") Long authorId, @RequestBody Book book) {
        return bookService.createBook(authorId, book);
    }

    @GetMapping(value = "/books/{bookId}")
    public Optional<Book> getBookById(@PathVariable(value = "bookId") Long bookId) {
        return bookService.getBookById(bookId);
    }

    @PutMapping(value = "/books")
    public Book updateBook(@PathVariable(value = "bookId") Long bookId, @RequestBody Book book) {
        return bookService.updateBookById(bookId, book);
    }

    @DeleteMapping(value = "/books/{bookId}")
    public ResponseEntity<Object> deleteBookById(@PathVariable(value = "bookId") long bookId) {
        return bookService.deleteBookById(bookId);
    }

    @PostMapping(value = "/{parentId}/childs")
    public Child createChild(@PathVariable(value = "parentId") Long parentId, @RequestBody Child child) {
        return childService.createChild(parentId, child);
    }

    @GetMapping(value = "/childs/{childId}")
    public Optional<Child> getChildById(@PathVariable(value = "childId") Long childId) {
        return childService.getChildById(childId);
    }

}
