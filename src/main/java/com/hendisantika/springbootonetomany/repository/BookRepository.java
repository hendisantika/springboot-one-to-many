package com.hendisantika.springbootonetomany.repository;

import com.hendisantika.springbootonetomany.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-one-to-many
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/03/20
 * Time: 13.39
 */
public interface BookRepository extends JpaRepository<Book, Long> {
}
