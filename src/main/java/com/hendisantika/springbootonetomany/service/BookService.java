package com.hendisantika.springbootonetomany.service;

import com.hendisantika.springbootonetomany.entity.Author;
import com.hendisantika.springbootonetomany.entity.Book;
import com.hendisantika.springbootonetomany.exception.ResourceNotFoundException;
import com.hendisantika.springbootonetomany.repository.AuthorRepository;
import com.hendisantika.springbootonetomany.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-one-to-many
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/03/20
 * Time: 13.44
 */
@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private AuthorRepository authorRepository;

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public Optional<Book> getBookById(Long bookId) {
        if (!bookRepository.existsById(bookId)) {
            throw new ResourceNotFoundException("Book with id " + bookId + " not found");
        }
        return bookRepository.findById(bookId);
    }

    public Book createBook(Long authorId, Book book) {
        Set<Book> books = new HashSet<>();
        Author author1 = new Author();

        Optional<Author> byId = authorRepository.findById(authorId);
        if (!byId.isPresent()) {
            throw new ResourceNotFoundException("Author with id " + authorId + " does not exist");
        }
        Author author = byId.get();

        //tie Author to Book
        book.setAuthor(author);

        Book book1 = bookRepository.save(book);
        //tie Book to Author
        books.add(book1);
        author1.setBooks(books);

        return book1;

    }

    public Book updateBookById(Long bookId, Book bookRequest) {
        if (!bookRepository.existsById(bookId)) {
            throw new ResourceNotFoundException("Book with id " + bookId + " not found");
        }
        Optional<Book> book = bookRepository.findById(bookId);

        if (!book.isPresent()) {
            throw new ResourceNotFoundException("Book with id " + bookId + " not found");
        }

        Book book1 = book.get();
        book1.setGenre(bookRequest.getGenre());
        book1.setTitle(bookRequest.getTitle());

        return bookRepository.save(book1);
    }

    public ResponseEntity<Object> deleteBookById(long bookId) {
        if (!bookRepository.existsById(bookId)) {
            throw new ResourceNotFoundException("Book with id " + bookId + " not found");
        }

        bookRepository.deleteById(bookId);

        return ResponseEntity.ok().build();

    }

}
