package com.hendisantika.springbootonetomany.service;

import com.hendisantika.springbootonetomany.entity.Author;
import com.hendisantika.springbootonetomany.exception.ResourceNotFoundException;
import com.hendisantika.springbootonetomany.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-one-to-many
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/03/20
 * Time: 13.41
 */
@Service
public class AuthorService {
    @Autowired
    private AuthorRepository authorRepository;

    public List<Author> getAuthors() {
        return authorRepository.findAll();
    }

    public Optional<Author> getAuthorById(Long authorId) {
        if (!authorRepository.existsById(authorId)) {
            throw new ResourceNotFoundException("Author with id " + authorId + " not found");
        }
        return authorRepository.findById(authorId);
    }

    public Author createAuthor(Author author) {
        return authorRepository.save(author);
    }

    public Author updateAuthorById(Long authorId, Author authorRequest) {
        if (!authorRepository.existsById(authorId)) {
            throw new ResourceNotFoundException("Author with id " + authorId + " not found");
        }
        Optional<Author> author = authorRepository.findById(authorId);

        if (!author.isPresent()) {
            throw new ResourceNotFoundException("Author with id " + authorId + " not found");
        }

        Author author1 = author.get();
        author1.setFirstName(authorRequest.getFirstName());
        author1.setLastName(authorRequest.getLastName());
        return authorRepository.save(author1);

    }

    public ResponseEntity<Object> deleteAuthorById(long authorId) {
        if (!authorRepository.existsById(authorId)) {
            throw new ResourceNotFoundException("Author with id " + authorId + " not found");
        }

        authorRepository.deleteById(authorId);

        return ResponseEntity.ok().build();

    }
}
