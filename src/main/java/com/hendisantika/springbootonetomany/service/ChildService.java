package com.hendisantika.springbootonetomany.service;

import com.hendisantika.springbootonetomany.entity.Child;
import com.hendisantika.springbootonetomany.entity.Parent;
import com.hendisantika.springbootonetomany.exception.ResourceNotFoundException;
import com.hendisantika.springbootonetomany.repository.ChildRepository;
import com.hendisantika.springbootonetomany.repository.ParentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-one-to-many
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/03/20
 * Time: 07.41
 */
@Service
public class ChildService {

    @Autowired
    private ChildRepository childRepository;

    @Autowired
    private ParentRepository parentRepository;

    public Optional<Child> getChildById(Long childId) {
        if (!childRepository.existsById(childId)) {
            throw new ResourceNotFoundException("Book with id " + childId + " not found");
        }
        return childRepository.findById(childId);
    }

    public Child createChild(Long childId, Child child) {
        Set<Child> children = new HashSet<>();
        Parent parent = new Parent();

        Optional<Parent> byId = parentRepository.findById(childId);
        if (!byId.isPresent()) {
            throw new ResourceNotFoundException("Parent with id " + childId + " does not exist");
        }
        Parent parent1 = byId.get();

        //tie Author to Book
        child.setParent(parent1);

        Child child1 = childRepository.save(child);
        //tie Book to Author
        children.add(child1);
        parent.setChildren(children);

        return child1;

    }
}
